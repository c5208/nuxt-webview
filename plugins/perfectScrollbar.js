import Vue from 'vue'
import VuePerfectScrollbar from 'vue-perfect-scrollbar'

if (process.client) {
    Vue.component('VuePerfectScrollbar', VuePerfectScrollbar)
}
