import jwt from 'jsonwebtoken';

function initialization(context, inject) {
    const request = async function () {}

    request.sign = function (data) {
        return jwt.sign(data, "@@LineOA2020@@");
    }

    request.decode = function (data) {
        return jwt.decode(data);
    }

    context.$jwt = request
    inject('jwt', request)

}

export default initialization