import _ from "lodash";
import Vue from "vue";
import exceljs from "exceljs";
import * as fs from "file-saver";
const libs = {

  addComma(value) {
    if (value) {
      value = value.toString();
      return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return value
    }

  },

  highlight(text, keyword, sensitive = true, styleClass = "highlight-search") {
    if (!keyword) return text;
    var searchMask = keyword.trim();
    var regEx = new RegExp(searchMask, sensitive ? "ig" : "g");
    var result = text.replace(regEx, match => {
      return `<span class='${styleClass}'>${match}</span>`;
    });

    return result;
  },

  toHHMMSS(time) {
    var sec_num = parseInt(time, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
  },

  displayTextHover(text, limit) {
    if (text && text.length < limit) {
      return ""
    }
    return text
  },
  displayEllipsisName(text, number, limit) {
    if (text && text.length > limit) {
      return text.substring(0, number) + "...";
    }
    return text;
  },
  exportReportSummary(dataSet) {
    let workbook = new exceljs.Workbook();
    let worksheet = workbook.addWorksheet(dataSet.title);
    let firstIndexTable, columnEnd, tbIndexSum, tbIndexLog, size ,startIndex, endIndex;
    let subTitle = dataSet.type == "hour" ? ' per hour' : dataSet.type == "day" ? ' per day' : ""

    worksheet.insertRow(1, [dataSet.title + subTitle])
    worksheet.getRow(1).font = { bold: true };

    if (dataSet.nameAgent && dataSet.nameAgent.length > 0) {
      worksheet.insertRow(2, [dataSet.nameAgent])
      worksheet.insertRow(3, [dataSet.dateRange])
      worksheet.getRow(2).font = { bold: true };
      worksheet.getRow(3).font = { bold: true };
      tbIndexSum = 5
      tbIndexLog = 13
    } else {
      worksheet.insertRow(2, [dataSet.dateRange])
      worksheet.getRow(2).font = { bold: true };
      tbIndexSum = 4
      tbIndexLog = 12
    }

      // set case agent add border
      if (dataSet.type == "agent"){
        startIndex =  tbIndexSum 
        endIndex =  9
      }else {
        startIndex = dataSet.nameAgent && dataSet.nameAgent.length ? tbIndexSum + 1 : tbIndexSum
        endIndex = dataSet.nameAgent && dataSet.nameAgent.length ? 11 : 10
      }

    if (dataSet.type == "all") {
      worksheet.addTable({
        name: "MyTable",
        ref: 'A' + tbIndexSum,
        headerRow: true,
        totalsRow: false,
        style: {
          theme: "TableStyleLight1",
        },
        columns: dataSet.column_table,
        rows: dataSet.rows_table,
      });
    } else {
      worksheet.addTable({
        name: "MyTable",
        ref: 'A' + tbIndexSum,
        headerRow: true,
        totalsRow: false,
        style: {
          theme: "TableStyleLight1",
        },
        columns: dataSet.column,
        rows: dataSet.rows,
      });

      worksheet.addTable({
        name: "MyTable",
        ref: 'A' + tbIndexLog,
        headerRow: true,
        totalsRow: false,
        style: {
          theme: "TableStyleLight1",
        },
        columns: dataSet.column_table,
        rows: dataSet.rows_table,
      });
      // set header Summary table
      worksheet.getRow(tbIndexSum).font = { bold: true };
      worksheet.getCell('A' + tbIndexSum).alignment = {
        vertical: "middle",
        horizontal: "center",
      };
      worksheet.getCell('B' + tbIndexSum).alignment = {
        vertical: "middle",
        horizontal: "center",
      };

      // set Summary table horizontal
      for (let i = startIndex; i <= endIndex; i++) {
        worksheet.getCell(`${"B" + i}`).alignment = {
          vertical: "middle",
          horizontal: "right",
        };
      }

      // set Summary table border color
      for (let i = tbIndexSum; i <= endIndex; i++) {
        worksheet.getCell(`${"A" + i}`).border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        worksheet.getCell(`${"B" + i}`).border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
      }
    }

    //set ค่าตัวเลขที่ต้องใช้ตั้งค่าตาราง ที่ต่างกันตามประเภทที่รับเข้ามา
    if (dataSet.type == "all") {
      firstIndexTable = tbIndexSum;
      columnEnd = 72;
      size = firstIndexTable + dataSet.rows_table.length

    }else if (dataSet.type == "hour") {
      firstIndexTable = tbIndexLog;
      columnEnd = 70;
      size = tbIndexLog + dataSet.rows_table.length
    } else {
      firstIndexTable = tbIndexLog;
      columnEnd = 71;
      size = tbIndexLog + dataSet.rows_table.length
    }

    // set border color and alignment of main table 
    worksheet = addBorderColorAlignment(size, firstIndexTable, columnEnd, worksheet)

    // set width Column
    worksheet = setWidthColumn(dataSet.type, worksheet)


    // genarate file xls
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(blob, dataSet.fileName );
    });
  },
  exportReportLog(dataSet) {
    let workbook = new exceljs.Workbook();
    let worksheet = workbook.addWorksheet(dataSet.title);
    let columnEnd, firstIndexTable;

    worksheet.insertRow(1, [dataSet.headers])
    worksheet.insertRow(2, [dataSet.dateRange])

    // worksheet.addTable({
    //   name: "MyTable",
    //   ref: 'A4',
    //   headerRow: true,
    //   totalsRow: false,
    //   style: {
    //     theme: "TableStyleLight1",
    //   },
    //   columns: dataSet.column_table,
    //    rows: dataSet.rows_table
    // });

    // worksheet.getRow(1).font = { bold: true };
    // worksheet.getRow(2).font = { bold: true };
    // worksheet.getCell('A1').alignment = {
    //   vertical: "middle",
    //   horizontal: "left",
    // };
    // worksheet.getCell('B2').alignment = {
    //   vertical: "middle",
    //   horizontal: "left",
    // };

    // let size = 4 + dataSet.rows_table.length
    // worksheet = addBorderColorAlignment(size, 4, 68, worksheet)

    // set width Column
    // worksheet = setWidthColumn(dataSet.type, worksheet)
    // var dobCol = worksheet.getColumn(1);
    // dobCol.width = 30;
    // var dobCol = worksheet.getColumn(4); 
    // dobCol.width = 25;
    //////////////////////////////// Flow เก่า ///////////////////////////////////////////

    //set hearder
    worksheet.getRow(1).font = { bold: true };
    worksheet.getRow(2).font = { bold: true };
    worksheet.getCell('A1').alignment = {
      vertical: "middle",
      horizontal: "left",
    };
    worksheet.getCell('B2').alignment = {
      vertical: "middle",
      horizontal: "left",
    };

    // set table
    worksheet.insertRow(4, dataSet.column_table)
    worksheet.getRow(4).alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    worksheet.getRow(4).font = { bold: true };

    firstIndexTable = 5
    dataSet.rows_table.map((row, index) => {
      worksheet.insertRow(firstIndexTable, row)
      firstIndexTable += 1
    })



    // set with column
    var dobCol = worksheet.getColumn(1);
    dobCol.width = 25;
    var dobCol = worksheet.getColumn(2);
    dobCol.width = 25;
    var dobCol = worksheet.getColumn(3);
    dobCol.width = 25;

    if (dataSet.type == "incoming" || dataSet.type == "outgoing") {
      columnEnd = 69
      var dobCol = worksheet.getColumn(4);
      dobCol.width = 25;
      var dobCol = worksheet.getColumn(5);
      dobCol.width = 30

    } else if (dataSet.type != "bot") {
      columnEnd = 68
      var dobCol = worksheet.getColumn(4);
      dobCol.width = 25;

    } else {
      columnEnd = 67
    }

    let size = 4 + dataSet.rows_table.length
    worksheet = addBorderColorAlignment(size, 4, columnEnd, worksheet)

    // mergeCells
    if (dataSet.type == "incoming" || dataSet.type == "outgoing") {
      for (let i = 0; i < dataSet.merge_index.length; i += 2) {
        // let index = dataSet.merge_index[i] +3
        let startIndex = dataSet.merge_index[i] + 4
        let endIndex = dataSet.merge_index[i + 1] + 4
        worksheet.mergeCells(`A${startIndex}:A${endIndex}`);
        worksheet.mergeCells(`B${startIndex}:B${endIndex}`);
        worksheet.mergeCells(`C${startIndex}:C${endIndex}`);
      }
    }

    // genarate file xls
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(blob, dataSet.fileName);
    });
  },
  exportLogQueue(column, rows, column_table, rows_table, column_table_agent, rows_table_agent, fileName) {

    let workbook = new exceljs.Workbook();
    let worksheet = workbook.addWorksheet('Queue Performance');

    worksheet.addTable({
      name: "MyTable",
      ref: "A1",
      headerRow: true,
      totalsRow: false,
      style: {
        theme: "TableStyleLight1",
      },
      columns: column,
      rows: rows,
    });


    worksheet.addTable({
      name: "MyTable",
      ref: "A8",
      headerRow: true,
      totalsRow: false,
      style: {
        theme: "TableStyleLight1",
      },
      columns: column_table,
      rows: rows_table,
    });

    // genarate file xls
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      });
      fs.saveAs(blob, fileName + ".xls");
    });
  }


};
function addBorderColorAlignment(size, firstIndexTable, columnEnd, worksheet) {
  for (let i = firstIndexTable; i <= size; i++) {
    for (let startChar = 65; startChar <= columnEnd; startChar++) {
      var res = String.fromCharCode(startChar);
      worksheet.getCell(`${res}${i}`).alignment = {
        vertical: "middle",
        horizontal: "center",
      };
      worksheet.getCell(`${res}${i}`).border = {
        top: { style: "thin" },
        left: { style: "thin" },
        bottom: { style: "thin" },
        right: { style: "thin" },
      };
    }
  }
  return worksheet
}

function setWidthColumn(type, worksheet) {

  var dobCol = worksheet.getColumn(1);
  dobCol.width = 25;
  var dobCol = worksheet.getColumn(2);
  dobCol.width = 17;
  var dobCol = worksheet.getColumn(3);
  dobCol.width = 17;
  var dobCol = worksheet.getColumn(4);
  dobCol.width = 17;
  var dobCol = worksheet.getColumn(5);
  dobCol.width = 17;
  var dobCol = worksheet.getColumn(6);
  dobCol.width = 17;
  var dobCol = worksheet.getColumn(7);
  dobCol.width = 17;
  // if (type == 'chat') {
  //   dobCol.width = 17;
  // } else {
  //   dobCol.width = 15;
  // }
  if (type == 'all') {
    var dobCol = worksheet.getColumn(8);
    dobCol.width = 15;
  }
  return worksheet
}
const plugin = {
  install() {
    Vue.prototype.$libs = libs;
  }
};

Vue.use(plugin);


