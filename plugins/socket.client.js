import Vue from 'vue'
import io from 'socket.io-client'
import VueSocketIO from 'vue-socket.io-extended'

export default ({ store, env, $config }) => {
    const ioInstance = io($config.socketHost, {
        reconnection: true,
        reconnectionDelay: 500,
        maxReconnectionAttempts: Infinity,
        autoConnect: false,
        transports: ["websocket"],
        upgrade : true
    });

    Vue.use(VueSocketIO, ioInstance, { store, actionPrefix: 'SOCKET_', eventToActionTransformer: (actionName) => actionName })

}