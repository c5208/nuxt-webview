
import https from 'https'

export default function ({ $axios, redirect, app, route }) {
  $axios.defaults.httpsAgent = new https.Agent({
    rejectUnauthorized: false
  })

  $axios.interceptors.request.use(
    (config) => {
      // console.log(config.url.search('CheckDuplicateInput'))
      if (config.url.search('CheckDuplicateInput') === -1) {
        app.store.dispatch('setLoadingOverlay', true)
      }
      return config
    },
    (error) => {
      return Promise.reject(error)
    }
  )

  $axios.interceptors.response.use(
    (response) => {
      app.store.dispatch('setLoadingOverlay', false)
      return response
    },
    (error) => {
      app.store.dispatch('setLoadingOverlay', false)
      return Promise.reject(error)
    }
  )
  // $axios.setToken(app.$auth.getToken('local'), 'Bearer')
  // $axios.setHeader('Content-Length', 20000000)
}
