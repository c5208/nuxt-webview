function initialization(context, inject) {
    const request = async function (config, headers) {
        try {
            const response = await context.$axios(config)
            return response.data;
        } catch (err) {
            if (err.response.status === 401 && (err.response.data.message === 'Unauthorization' || err.response.data.message === 'Token_Expired')) {
                context.$auth.logout();
                context.redirect('/')
            }
            return err.response.data;

        }
    }

    request.$get = function (path, params, config, headers) {
        return request(mergeConfig({ method: 'get', url: path, params: params, headers }, config,))
    }

    request.$post = function (path, data, config, headers) {
        return request(mergeConfig({ method: 'post', url: path, data: data, headers }, config))
    }

    request.$patch = function (path, data, config, headers) {
        return request(mergeConfig({ method: 'patch', url: path, data: data, headers }, config,))
    }

    request.$put = function (path, data, config, headers) {
        return request(mergeConfig({ method: 'put', url: path, data: data, headers }, config))
    }


    request.$delete = function (path, data, config, headers) {
        return request(mergeConfig({ method: 'delete', url: path, data: data, headers }, config))
    }


    function mergeConfig(base, extra) {
        if (typeof extra === 'object' && extra !== null) Object.assign(base, extra)
        return base
    }


    context.$api = request
    inject('api', request)
}

export default initialization
