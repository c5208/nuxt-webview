import $helpers from "../../helpers/FunctionHelper.helper"
import _ from "lodash"
/**
 * initial state
 */
const getDefaultState = () => {
    return {
        connect_list: [{
            channel_type_info: { _id: "", name: "Line" },
            company_id: "",
            line_id: "",
            name: "",
            pictureUrl: "",
        }],
        broadcast_chart: {
            num_use_day_current: 0,
            num_limit: 0
        },
        graph: {
            format: "",
            pointFormat: "",
            yAxisTitle: "",
            categories: [],
            colors: ["#01B901"],
            series: [
                {
                    name: "Line",
                    data: [],
                },
            ]
        },
        contacts: [{
            type: "Line",
            data: {
                current_insight: {
                    blocks: 0,
                    followers: 0,
                    status: "ready",
                    targetedReaches: 0,
                },
                growPercent: {
                    blocks: {
                        diff: 0,
                        percent: 100,
                        type: "positive",
                    },
                    followers: {
                        diff: 0,
                        percent: 100,
                        type: "positive",
                    },
                    targetedReaches: {
                        diff: 0,
                        percent: 100,
                        type: "positive",
                    },

                }
            }
        }],
        dashboard_box_one: [
            {
                iconClass: "chat",
                title: "Total chats",
                title_th: "แชททั้งหมด",
                key: "totalChat",
                value: "0",
            },
            {
                iconClass: "chat-incoming",
                title: "Incoming chats",
                title_th: "แชทที่รับเข้ามา",
                key: "incommingChat",
                value: "0",
            },
            {
                iconClass: "chat-outgoing",
                title: "Outgoing chats",
                title_th: "แชทที่ส่งออปไป",
                key: "outgoingChat",
                value: "0",
            },
            {
                iconClass: "missed",
                title: "Missed chats",
                title_th: "พลาดรับแชท",
                key: "missedChat",
                value: "0",
            },
        ],
        dashboard_box_two: [
            {
                iconClass: "chat-with-agent",
                title: "Avg chat / Agent",
                title_th: "อัตราแชทเฉลี่ยต่อเจ้าหน้าที่",
                key: "avgAgentChat",
                value: "0",
            },
            {
                iconClass: "timer",
                title: "Avg response time",
                title_th: "อัตราเวลาเฉลี่ย",
                key: "avgRespTime",
                value: "00:00:00",
            },
            {
                iconClass: "timer",
                title: "Avg chat duration",
                title_th: "อัตราระยะเวลาการแชท",
                key: "avgChatDuration",
                value: "00:00:00",
            },
            {
                iconClass: "missed-expired",
                title: "Chats expired",
                title_th: "แชทหมดอายุ",
                key: "expiredChat",
                value: "0",
            },
        ],
    }
}

const state = getDefaultState;

/**
 * getters
 */
const getters = {
    graph: (state) => state.graph,
    broadcast_chart: (state) => state.broadcast_chart,
    dashboard_box_one: (state) => state.dashboard_box_one,
    dashboard_box_two: (state) => state.dashboard_box_two,
    contacts: (state) => state.contacts,
    connect_list: (state) => state.connect_list
};

/**
 * actions
 */
const actions = {
    async getDashboard({ commit, dispatch, state }) {
        try {
            const { response_status, result } = await this.$api.$get(`/api/lists`);
            console.log(response_status)
            let resRetuen = []
            if (response_status.code == '00000') {
                for (const key in result) {
                    let obj = {
                        _id: result[key]._id,
                        "name": result[key].data[0].name,
                        "symbol": result[key].data[0].symbol,
                        "slug": result[key].data[0].slug,
                        quote: result[key].data[0].quote.THB,
                        graph: []
                    }
                    let datas = _.cloneDeep(_.uniqBy(result[key].data,'last_updated'))
                    for (const key2 in datas) {
                        let timeg = this.$dayjs(datas[key2].last_updated).valueOf()
                        let price = datas[key2].quote.THB.price
                        obj.graph.push([timeg,price])
                    }
                    
                    resRetuen.push(obj)
                }
                resRetuen = _.orderBy(resRetuen, ['name'],['asc']), 
                console.log(resRetuen)
                commit("setState", { key: "graph", data: resRetuen });
                return resRetuen;

            } else {
                return []
            }
        } catch (error) {
            console.log(error)
        }
    },
};

/**
 * mutations
 */
const mutations = {
    resetState(state) {
        Object.assign(state, getDefaultState())
    },
    setState(state, opts) {
        state[opts.key] = opts.data;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
