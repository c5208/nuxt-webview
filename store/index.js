import dashboard from "./modules/dashboard";
export const state = () => ({
    menuIsFull: true,
    dateNow: Date.now(),
    server_timestamp_distance: 0,
    intervalTimer: null,
    disabledButton: false,
    logo: require(`assets/images/logoclicknext2.png`),
    overlay: false,
    dateRange: {
        startDate: new Date(),
        endDate: new Date(),
        type: 'today'
    },
    lang: 'en',
    globalLoading: false,
    toggleSideBar: false,
})

export const getters = {
    menuToggle: (state) => state.menuIsFull,
    dateNow: (state) => state.dateNow,
    isDisabledButton: (state) => state.disabledButton,
    logo: (state) => state.logo,
    overlay: (state) => state.overlay,
    dateRange: (state) => state.dateRange,

}

export const actions = {
    initInterval({ commit, state, dispatch }) {
        clearInterval(state.intervalTimer);
        state.intervalTimer = setInterval(() => {
            commit("updateInterval");
            dispatch("authenticate/AUTH_VALID_EXPIRED_TOKEN");
        }, 1000);
    },
    setToggleSideBar({ commit }, value) {
        commit("setState", { key: "toggleSideBar", data: value })
    },
    setLoadingOverlay({ commit, state }, action) {
        // console.log(action);
        commit("setLoading", (action = action));
    },
    setDateRange({ commit, state }, dateRange) {
        localStorage.setItem("dateRange", JSON.stringify(dateRange));
        commit("setState", {
            key: "dateRange",
            data: dateRange,
        });
    },
    changeLang({ commit }, opt = null) {
        localStorage.setItem('lang', opt)
        commit('setLang', opt);
    },
    setGlobalLoading({ commit }, data) {
        commit('setState', { key: "globalLoading", data: data });
    }
}

export const mutations = {
    changeToggle(state) {
        state.menuIsFull = !state.menuIsFull;
    },
    changeToggleDisabledButton(state, action) {
        state.disabledButton = action;
    },
    clearInterval(state) {
        clearInterval(state.intervalTimer);
    },
    updateInterval(state) {
        state.dateNow = Date.now() - state.server_timestamp_distance;
    },
    setServerTimestamp(state, { server_timestamp }) {
        state.server_timestamp_distance = Date.now() - server_timestamp;
    },
    setLoading(state, action) {
        // console.log(action);
        state.overlay = action;
    },
    setState(state, opts) {
        state[opts.key] = opts.data;
    },
    setLang(state, opt) {
        state.lang = opt
    }
}

export const modules = {
    dashboard
}


// export const strict = false