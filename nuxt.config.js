const path = require("path");
const routeIndex = require('./routes/index')

export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: "Dashboard",
        htmlAttrs: {
            lang: "en",
        },
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            { hid: "description", name: "description", content: "" },
            { name: "format-detection", content: "telephone=no" },
        ],
        link: [{ rel: "icon", type: "image/x-icon", href: "/chatcone.ico" }],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '@/assets/scss/style.bundle.scss',
    ],

    script: [
        { src: "https://code.highcharts.com/highcharts.js" },
        { src: "https://code.highcharts.com/modules/data.js" },
        { src: "https://code.highcharts.com/modules/drilldown.js" },
        { src: "https://code.highcharts.com/modules/exporting.js" },
        { src: "https://code.highcharts.com/modules/export-data.js" },
        { src: "https://code.highcharts.com/modules/accessibility.js" },
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: '~/plugins/helpers.js' },
        { src: '~/plugins/joi.js' },
        { src: '~/plugins/socket.client.js', ssr: false },
        { src: '~/assets/plugins/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css' },
        { src: "~/helpers/helpers.js", ssr: false },
        { src: '~/plugins/api.js' },
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/bootstrap
        "bootstrap-vue/nuxt",
        // https://go.nuxtjs.dev/axios
        "@nuxtjs/axios",
        '@nuxtjs/proxy',
        '@nuxtjs/dayjs',
        '@nuxtjs/dotenv',
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        proxy: true, // Can be also an object with default options
        retry: { retries: 3, retryDelay: () => 3000 },
    },
    dayjs: {
        locales: ['en'],
        defaultLocale: 'en',
        plugins: ["calendar"]
    },
    proxy: {
        '/api': { target: process.env.NODE_API_HOST, ws: false },
    },
    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        terser: {
            extractComments: true,
            sourceMap: true,
            parallel: true,
            cache: true,
            terserOptions: {
                extractComments: 'all',
                compress: {
                    drop_console: true,
                    drop_debugger: true
                }
            }
        },
        // extractCSS: true,
        extend(config, ctx) {
            config.resolve.alias['@components'] = path.join(__dirname, 'components')
            config.resolve.alias['@store'] = path.join(__dirname, 'store')
            config.resolve.alias['@assets'] = path.join(__dirname, 'assets')
            config.resolve.alias['@images'] = path.join(__dirname, 'assets/images')
            config.resolve.alias['@services'] = path.join(__dirname, 'services')
            config.resolve.alias['@helpers'] = path.join(__dirname, 'helpers')
            config.resolve.alias['@pages'] = path.join(__dirname, 'pages')
            config.resolve.alias['@plugins'] = path.join(__dirname, 'assets/plugins')
            config.output.globalObject = 'this'
            config.module.rules.push(
                {
                    test: /\.pdf$/,
                    loader: 'url-loader'
                }
            )
            if (ctx.isDev) {
                config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
            }
        },
        
        babel: {
            compact: false
        },
        loaders: {
            vue: {
                prettify: false
            }
        }
    },
    router: {
        extendRoutes(nuxtRoutes, resolve) {
            nuxtRoutes.splice(0, nuxtRoutes.length, ...routeIndex.map((route) => {
                return {
                    ...route,
                    component: route.component ? resolve(__dirname, route.component) : undefined
                }
            }))
        }

    },
    server: {
        port: process.env.PORT, // default: 3000
        host: process.env.HOST, // default: localhost,
        timing: false
    },
};
