import { css } from "vue-styled-components";
const getStyleFlex = ({ flex, parentLayout }) => {
    if (flex === undefined) return ``;
    if (flex === 0) {
        return css`
    ${parentLayout === "horizontal" || parentLayout === "baseline"
                ? "width: auto;"
                : ""
            }
      -webkit-box-flex: ${flex};
      flex: none;
      /* -webkit-box-flex: 0!important;
    flex: none!important; */
    `;
    } else {
        return css`
      -webkit-box-flex: ${flex} !important;
      flex: ${flex} 0 0 !important;
    `;
    }
};
const getStylePosition = ({ position }) => {
    return css`
    position: ${position || "relative"};
  `;
};
const getStyleOffset = ({
    offsetTop,
    offsetBottom,
    offsetStart,
    offsetEnd,
}) => {
    return css`
  ${offsetTop &&
        css`
      top: ${offsetTop};
    `}
  ${offsetBottom &&
        css`
      bottom: ${offsetBottom};
    `}
  ${offsetStart &&
        css`
      left: ${offsetStart};
    `}
  ${offsetEnd &&
        css`
      right: ${offsetEnd};
    `}
  `;
};
const getMargin = (direction, datasize, important = false) => {
    switch (datasize) {
        case "none":
            return `
        margin-${direction}: 0 ${important ? `!important` : ``};
      `;
        case "xs":
            return `
        margin-${direction}: 2px ${important ? `!important` : ``};
      `;
        case "sm":
            return `
        margin-${direction}: 4px ${important ? `!important` : ``};
      `;
        case "md":
            return `
        margin-${direction}: 8px ${important ? `!important` : ``};
      `;
        case "lg":
            return `
        margin-${direction}: 12px ${important ? `!important` : ``};
      `;
        case "xl":
            return `
        margin-${direction}: 16px ${important ? `!important` : ``};
      `;
        case "xxl":
            return `
        margin-${direction}: 20px ${important ? `!important` : ``};
      `;

        default:
            return `
        margin-${direction}: 0;
      `;
    }
};
const InheritLayout = ({ parentLayout }) => {
    switch (parentLayout) {
        case "horizontal":
            return css`
        width: 0;
        overflow: hidden;
        -webkit-box-flex: 1;
        flex: 1 1 auto;
      `;
        case "vertical":
            return css`
        -webkit-box-flex: 0;
        flex: none;
      `;
        case "baseline":
            return css`
        width: 0;
        overflow: hidden;
        -webkit-box-flex: 1;
        flex: 1 1 auto;
      `;

        default:
            return css``;
    }
};
const InheritWidthFromLayout = ({ rootAbsolute, parentLayout, flex }) => {
    if (
        (parentLayout === "horizontal" && rootAbsolute) ||
        (parentLayout === "horizontal" && flex === 0)
    )
        return css`
      width: auto;
    `;
};
const getStyleSize = ({ styleWidth, styleHeight }) => {
    const cssWidth = css`
    width: ${styleWidth} !important;
  `;
    const cssHeight = css`
    height: ${styleHeight} !important;
  `;
    return css`
    ${styleWidth ? cssWidth : ``}
    ${styleHeight ? cssHeight : ``}
   ${styleWidth || styleHeight
            ? css`
           -webkit-box-flex: 0 !important;
           flex: none !important;
         `
            : ``
        }
  `;
};
const getGravity = (gravity) => {
    switch (gravity) {
        case "bottom":
            return css`
        -webkit-box-pack: end;
        flex-pack: end;
        justify-content: flex-end;
      `;

        default:
            return css`
        -webkit-box-pack: ${gravity};
        flex-pack: ${gravity};
        justify-content: ${gravity};
      `;
    }
};

export {
    getStyleFlex,
    getStylePosition,
    getStyleOffset,
    getMargin,
    InheritLayout,
    InheritWidthFromLayout,
    getStyleSize,
    getGravity,
};
