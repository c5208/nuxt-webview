const ReplyMessage = [
    {
        type: "text",
        show_type: "Text",
        iconClass: "far fa-comment",
        icon: "icon-text-messge",
        name: "",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        default_data: {
            type: "text",
            text: "Hello",
        },
        data_dynamic: {
            url: "",
            method: "GET",
            request: "{ }",
            header: "{ }",
            body: "{ }",
            response: "{ }",
        },
        data_normal: {
            text: "Hello",
        },
    },

    {
        type: "file",
        icon: "icon-attach-file",
        show_type: "Attach File",
        iconClass: "far fa-smile",
        name: "file",
        mode: "normal",
        iconWidth: "14px",
        iconHeight: "16px"
    },
    {
        type: "sticker",
        show_type: "Sticker",
        icon: "icon-sticker",
        iconClass: "far fa-smile",
        name: "Sticker",
        mode: "normal",
    },
    {
        type: "location",
        show_type: "Location",
        icon: "icon-location",
        iconClass: "fas fa-map-marker-alt",
        name: "",
        mode: "normal",
    },

    {
        type: "image",
        show_type: "Image",
        name: "",
        iconClass: "far fa-image",
        iconHeight: "16px",
        icon: "icon-image",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            image: "",
            altText: "Alt TEXT",
            type: "customer_text",
            message: "",
            tempImage: [],
        },
        data_dynamic: null,
        default_data: {
            type: "image",
            originalContentUrl: "",
            previewImageUrl: "",
        },
    },
    {
        type: "imageMap",
        show_type: "ImageMap",
        name: "",
        iconClass: "far fa-image",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            image: "",
            altText: "Alt TEXT",
            type: "customer_text",
            message: "",
            tempImage: [],
        },
        data_dynamic: null,
        default_data: {
            type: "imagemap",
            baseUrl: "",
            baseSize: {
                height: 0,
                width: 1040,
            },
        },
    },
    {
        type: "button",
        show_type: "Button",
        name: "",
        iconClass: "far fa-bars",

        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            title: "",
            text: "",
            image: "",
            tempImage: [],
            numberActions: 1,
            actions: [
                {
                    type: "customer_text",
                    message: "Action 1",
                    buttonName: "Action 1",
                },
            ],
        },
        data_dynamic: null,
        default_data: {
            type: "flex",
            altText: "Flex Message",
            contents: {
                type: "bubble",
                direction: "ltr",
                hero: {
                    type: "image",
                    url: "",
                    size: "full",
                    aspectRatio: "1.51:1",
                    aspectMode: "fit",
                },
                footer: {
                    type: "box",
                    layout: "vertical",
                    contents: [
                        {
                            type: "button",
                            action: {
                                type: "message",
                                label: "Action 1",
                                text: "Action 1",
                            },
                        },
                    ],
                },
            },
        },
    },
    {
        type: "bubble",
        show_type: "Bubble",
        name: "",
        iconClass: "far fa-square",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            title: "",
            text: "",
            image: "",
            tempImage: [],
            numberActions: 1,
            actions: [
                {
                    type: "customer_text",
                    message: "Action 1",
                    buttonName: "Action 1",
                },
            ],
        },
        data_dynamic: null,
        default_data: {
            type: "flex",
            altText: "Flex Message",
            contents: {
                type: "bubble",
                direction: "ltr",
                hero: {
                    type: "image",
                    url: "",
                    size: "full",
                    aspectRatio: "1.51:1",
                    aspectMode: "fit",
                },
                body: {
                    type: "box",
                    layout: "vertical",
                    contents: [
                        {
                            type: "text",
                            text: "",
                            align: "Lefy",
                        },
                        {
                            type: "text",
                            text: "",
                            size: "md",
                        },
                    ],
                },
                footer: {
                    type: "box",
                    layout: "vertical",
                    contents: [
                        {
                            type: "button",
                            action: {
                                type: "message",
                                label: "Action 1",
                                text: "Action 1",
                            },
                        },
                    ],
                },
            },
        },
    },
    {
        type: "carousel",
        show_type: "Carousel",
        iconClass: "far fa-clone",
        icon: "icon-carousel",
        name: "",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            altText: "Alt Text",
            bubbleIds: [""],
        },
        data_dynamic: null,
        default_data: {
            type: "flex",
            altText: "Flex Message",
            contents: {
                type: "carousel",
                contents: [],
            },
        },
    },
    {
        type: "video",
        icon: "icon-video",
        show_type: "Video",
        iconClass: "far fa-smile",
        name: "video",
        mode: "normal",
    },
    {
        type: "custom",
        show_type: "JSON",
        iconClass: "fas fa-code",
        icon: "icon-json",
        name: "",
        mode: "normal",
        is_system: false,
        is_active: true,
        system_type: null,
        data_normal: {
            json: "",
        },
        data_dynamic: null,
        default_data: null,
    },
    {
        type: "template_message",
        show_type: "Template Message",
        icon: "icon-carousel",
        iconClass: "fas fas fa-columns",
        name: "",
        mode: "normal",
    }, ,
    {
        type: "imagemap",
        show_type: "Rich Video",
        icon: "icon-rich-video",
        iconClass: "far fa-smile",
        name: "imagemap",
        mode: "normal",
    }
];
const ReplyMessageWidget = [
    {
        type: "text",
        show_type: "Text",
        iconClass: "far fa-comment",
        icon: "icon-text-messge",

    },
    {
        type: "image",
        show_type: "Image",
        name: "image",
        iconClass: "far fa-image",
        iconHeight: "16px",
        icon: "icon-image",
    },
    {
        type: "video",
        icon: "icon-video",
        show_type: "Video",
        iconClass: "far fa-smile",
        name: "video",
        mode: "normal",
    },
    {
        type: "file",
        icon: "icon-attach-file",
        show_type: "Attach File",
        iconClass: "far fa-smile",
        name: "file",
        mode: "normal",
        iconWidth: "14px",
        iconHeight: "16px"
    },
    // {
    //     type: "carousel",
    //     show_type: "Carousel",
    //     iconClass: "far fa-clone",
    //     icon: "icon-carousel",
    //     name: "",
    //     mode: "normal",
    //     is_system: false,
    //     is_active: true,
    //     system_type: null,
    // },
    // {
    //     type: "talk_to_human",
    //     icon: "icon-user",
    //     show_type: "Talk to human",
    //     // iconClass: "far fa-smile",
    //     name: "talk_to_human",
    //     mode: "normal",
    //     iconWidth: "14px",
    //     iconHeight: "16px"
    // },
    
]
const ReplyMessageFacebook = [
    {
        type: "text_and_button",
        show_type: "Text",
        iconClass: "far fa-comment",
        icon: "icon-text-messge",

    },
    {
        type: "generic_template",
        show_type: "Bubble/Carousel",
        iconClass: "far fa-clone",
        icon: "icon-carousel",
    },
    {
        type: "media",
        show_type: "Media",
        name: "media",
        iconClass: "far fa-image",
        iconHeight: "16px",
        icon: "icon-image",
        mode: "normal",
    },
    // {
    //     type: "image",
    //     show_type: "Image",
    //     name: "image",
    //     iconClass: "far fa-image",
    //     iconHeight: "16px",
    //     icon: "icon-image",
    //     mode: "normal",
    // },
    // {
    //     type: "video",
    //     icon: "icon-video",
    //     show_type: "Video",
    //     iconClass: "far fa-smile",
    //     name: "video",
    //     mode: "normal",
    // },
    {
        type: "file",
        icon: "icon-attach-file",
        show_type: "Attach File",
        iconClass: "far fa-smile",
        name: "file",
        mode: "normal",
        iconWidth: "14px",
        iconHeight: "16px"
    }
]
export default {
    getReplyMessageDefaultByType: (type) => {
        return ReplyMessage.filter((reply_msg) => reply_msg.type === type)[0];
    },
    getReplyMessageIconByType: (type) => {
        const reply_message = ReplyMessage.filter(
            (reply_msg) => reply_msg.type === type
        )[0];
        return reply_message;
    },
    getReplyMessageType: () => {
        return ReplyMessage.map((reply_msg) => ({
            type: reply_msg.type,
            show_type: reply_msg.show_type,
            iconClass: reply_msg.iconClass,
            icon: reply_msg.icon,
            iconHeight: reply_msg.iconHeight ? reply_msg.iconHeight : "18px",
            iconWidth: reply_msg.iconWidth ? reply_msg.iconWidth : "18px",
        }));
    },

    getReplyMessageTypeFacebook: () => {
        return ReplyMessageFacebook.map((reply_msg) => ({
            type: reply_msg.type,
            show_type: reply_msg.show_type,
            iconClass: reply_msg.iconClass,
            icon: reply_msg.icon,
            iconHeight: reply_msg.iconHeight ? reply_msg.iconHeight : "18px",
            iconWidth: reply_msg.iconWidth ? reply_msg.iconWidth : "18px",
        }));
    },


    getReplyMessageTypeWidget: () => {
        return ReplyMessageWidget.map((reply_msg) => ({
            type: reply_msg.type,
            show_type: reply_msg.show_type,
            iconClass: reply_msg.iconClass,
            icon: reply_msg.icon,
            iconHeight: reply_msg.iconHeight ? reply_msg.iconHeight : "18px",
            iconWidth: reply_msg.iconWidth ? reply_msg.iconWidth : "18px",
        }));
    }
};
