
const success = ($vs, options = {}) => {
    return $vs.notify({
        title: options.title ? options.title : "Success",
        text: options.text ? options.text : "save success",
        color: "success",
        position: "top-right",
        icon: "done",
        
    });
}

const warning = ($vs, options = {}) => {
    return $vs.notify({
        title: options.title ? options.title : "Success",
        text: options.text ? options.text : "save success",
        color: "warning",
        position: "top-right",
        icon: "done",
        
    });
}


const fail = ($vs, options = {}) => {
    return $vs.notify({
        title: options.title ? options.title : "Fail",
        text: options.text ? options.text : "save fail",
        color: "danger",
        position: "top-right",
        icon: "warning"
    });
}

export default { success, fail, warning };