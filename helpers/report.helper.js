export default {
    sortbyField:(type,sort_type,chat_volume_table)=>{
        let value = _.cloneDeep(chat_volume_table)
        try { 
            // console.log(sort_type)
            switch (type) {
              case "firstname":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.fullnameSort > b.fullnameSort ? 1 : b.fullnameSort > a.fullnameSort ? -1 : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.fullnameSort < b.fullnameSort ? 1 : b.fullnameSort < a.fullnameSort ? -1 : 0
                  );
                }
                break;
              case "date":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.date > b.date ? 1 : b.date > a.date ? -1 : 0
                  );
                } else if (sort_type == "descending") {
                   
                  value.data.sort((a, b) =>
                    a.date < b.date ? 1 : b.date < a.date ? -1 : 0
                  );
                }
                break;
              case "time":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.time > b.time ? 1 : b.time > a.time ? -1 : 0
                  );
                } else if (sort_type == "descending") {
                   
                  value.data.sort((a, b) =>
                    a.time < b.time ? 1 : b.time < a.time ? -1 : 0
                  );
                }
                break;
              case "bot_chat":
                if (sort_type == "ascending") {
                 
                  value.data.sort((a, b) =>
                    a.count_bot > b.count_bot ? 1 : b.count_bot > a.count_bot ? -1 : 0
                  );
                } else if (sort_type == "descending") {
                 
                  value.data.sort((a, b) =>
                    a.count_bot < b.count_bot ? 1 : b.count_bot < a.count_bot ? -1 : 0
                  );
                }
                break;
              case "incoming_chat":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.count_incoming > b.count_incoming
                      ? 1
                      : b.count_incoming > a.count_incoming
                      ? -1
                      : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.count_incoming < b.count_incoming
                      ? 1
                      : b.count_incoming < a.count_incoming
                      ? -1
                      : 0
                  );
                }
                break;
              case "outgoing_chats":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.count_outgoing > b.count_outgoing
                      ? 1
                      : b.count_outgoing > a.count_outgoing
                      ? -1
                      : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.count_outgoing < b.count_outgoing
                      ? 1
                      : b.count_outgoing < a.count_outgoing
                      ? -1
                      : 0
                  );
                }
                break;
              case "missed_chat":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.count_missed > b.count_missed
                      ? 1
                      : b.count_missed > a.count_missed
                      ? -1
                      : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.count_missed < b.count_missed
                      ? 1
                      : b.count_missed < a.count_missed
                      ? -1
                      : 0
                  );
                }
                break;
              case "chats_expired":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.count_expired > b.count_expired
                      ? 1
                      : b.count_expired > a.count_expired
                      ? -1
                      : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.count_expired < b.count_expired
                      ? 1
                      : b.count_expired < a.count_expired
                      ? -1
                      : 0
                  );
                }
                break;
              case "chat_utilization":
                if (sort_type == "ascending") {
                  value.data.sort((a, b) =>
                    a.chat_utilization > b.chat_utilization
                      ? 1
                      : b.chat_utilization > a.chat_utilization
                      ? -1
                      : 0
                  );
                } else if (sort_type == "descending") {
                  value.data.sort((a, b) =>
                    a.chat_utilization < b.chat_utilization
                      ? 1
                      : b.chat_utilization < a.chat_utilization
                      ? -1
                      : 0
                  );
                }
                break;
                case "awayDuration":
                  if (sort_type == "ascending") {
                    value.data.sort((a, b) =>
                      a.away_duration > b.away_duration
                        ? 1
                        : b.away_duration > a.away_duration
                        ? -1
                        : 0
                    );
                  } else if (sort_type == "descending") {
                    value.data.sort((a, b) =>
                      a.away_duration < b.away_duration
                        ? 1
                        : b.away_duration < a.away_duration
                        ? -1
                        : 0
                    );
                  }
                  break;
                  case "avg_resp_time":
                    if (sort_type == "ascending") {
                      value.data.sort((a, b) =>
                        a.avg_first_resp_time > b.avg_first_resp_time
                          ? 1
                          : b.avg_first_resp_time > a.avg_first_resp_time
                          ? -1
                          : 0
                      );
                    } else if (sort_type == "descending") {
                      value.data.sort((a, b) =>
                        a.avg_first_resp_time < b.avg_first_resp_time
                          ? 1
                          : b.avg_first_resp_time < a.avg_first_resp_time
                          ? -1
                          : 0
                      );
                    }
                    break;
                    case "avg_chat_duration":
                      if (sort_type == "ascending") {
                        value.data.sort((a, b) =>
                          a.avg_chat_duration > b.avg_chat_duration
                            ? 1
                            : b.avg_chat_duration > a.avg_chat_duration
                            ? -1
                            : 0
                        );
                      } else if (sort_type == "descending") {
                        value.data.sort((a, b) =>
                          a.avg_chat_duration < b.avg_chat_duration
                            ? 1
                            : b.avg_chat_duration < a.avg_chat_duration
                            ? -1
                            : 0
                        );
                      }
                      break;
              default:
                break;
            }
          
             console.log('value :',value)
            return value;
          } catch (error) {
            console.log("error", error);
          }
          
       
        
    }
};
