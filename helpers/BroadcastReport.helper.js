
const keyColumn = {
    "text":{
        Column : [ "Link url", "click", "click rate","user of click",],
        keyColumns :  [
            { key: "url", width: 20 },
            { key: "click", width: 26 },
            { key: "click_rate", width: 28, horizontal: "right" },
            { key: "user_click", width: 26, horizontal: "right" },
        ],
    },
    "bubble" :{
        Column : [ "Button", "Action","Click","Click rate","User of click"],
        keyColumns :  [
            { key: "Button", width: 20 },
            { key: "Action", width: 45,horizontal: "left" },
            { key: "Click", width: 26, horizontal: "left" },
            { key: "Click_rate", width: 26, horizontal: "left" },
            { key: "User_click", width: 26, horizontal: "left" },
        ],
    },
    "carousel" :{
        Column : [ "Card","Button", "Action","Click","Click rate","User of click"],
        keyColumns :  [
            { key: "Card", width: 20 },
            { key: "Button", width: 10 },
            { key: "Action", width: 45,horizontal: "left" },
            { key: "Click", width: 26, horizontal: "left" },
            { key: "Click_rate", width: 26, horizontal: "left" },
            { key: "User_click", width: 26, horizontal: "left" },
        ],
    },
    "location" :{
        Column : [ "Card", "Button", "Action","Click","Click rate","User of click"],
        keyColumns :  [
            { key: "Card", width: 20 },
            { key: "Button", width: 10 },
            { key: "Action", width: 45,horizontal: "left" },
            { key: "Click", width: 26, horizontal: "left" },
            { key: "Click rate", width: 26, horizontal: "left" },
            { key: "User_click", width: 26, horizontal: "left" },
        ],
    },
    "video" : {
        Column : ["Duration of play", "Number of view", "View rate", "User view",],
        keyColumns :   [
            { key: "Duration", width: 20 },
            { key: "number", width: 26 },
            { key: "rate", width: 28, horizontal: "left" },
            { key: "user_view", width: 26, horizontal: "left" },
          ]
    },
    "custom":{
        Column : ["Button", "Action", "Click", "click rate", "User of click",],
        keyColumns :   [
            { key: "Button", width: 20 },
            { key: "Action", width: 26 },
            { key: "click", width: 26 },
            { key: "click_rate", width: 28 },
            { key: "user_click", width: 26 },
          ]
    },


}
export default {
    getReportBroadcast : ()=>{
        return Report;
    },
    getColumnAndRowReportByType : (type)=>{
        return keyColumn[type]
    },
    getTagList : (temp)=>{
        let tag = {
            targetList: "",
            ExTargetList: ""
        }
        if (temp.messages_target.length > 0) {
            if(temp.messages_target[0]){
            let targetList = ""
            temp.messages_target[0].option.map((item, pos) => {
                if (item.text.length > 0) {
                    if (pos == 0) {
                        targetList = targetList + item.text;
                    } else {
                        targetList = targetList + "," + item.text;
                    }
                }
            });
            tag.targetList =   targetList
            }

            if(temp.messages_target[1]){
                let ExTargetList = ""
                temp.messages_target[1].option.map((item, pos) => {
                    if (item.text.length > 0) {
                        if (pos == 0) {
                            ExTargetList = ExTargetList + item.text;
                        } else {
                            ExTargetList = ExTargetList + "," + item.text;
                        }
                    }
                });
                tag.ExTargetList =   ExTargetList
            }
           
        }
        return tag
    },
    getFilterList : (temp)=> {
        let FilterList = []
           
        if (temp.messages_target.length > 0) {
            temp.messages_target.map((row)=>{
                let tag = ""
                row.option.map((item, pos) => {
                    if (item.text.length > 0) {
                        if (pos == 0) {
                            tag = tag + item.text;
                        } else {
                            tag = tag + "," + item.text;
                        }
                    }
                });

                FilterList.push({
                    FilterType: row.type,
                    FilterTag: tag
                })
            })
        }
        return FilterList
    }
}