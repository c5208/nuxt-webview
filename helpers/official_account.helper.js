const selectOA = ({ context, official_account }) => {
    context.$socket.emit("LEAVE_ROOM", context.selected_oa._id);
    context.$store.dispatch("authenticate/setSelectedOA", {
        selected_oa: official_account,
    });
    // context.$store.dispatch("authenticate/SOCKET_connect");
    context.$router.push("/").catch(err => { });
};

export { selectOA };
