import dayjs from "dayjs";
import calendar from "dayjs/plugin/calendar";
import dayOfYear from 'dayjs/plugin/dayOfYear';

dayjs.extend(calendar)
dayjs.extend(dayOfYear)

export default {
    getDateFromX: (date_current, date_from) => {
        const _current = dayjs(date_current);
        const _timestamp = dayjs(new Date(date_from * 1000).getTime());

        const diff_day = _current.dayOfYear() - _timestamp.dayOfYear();
        const diff_year = _current.diff(_timestamp, 'year')

        if (diff_day == 0 && diff_year == 0) {
            return _timestamp.format("HH:mm")
        } else if (diff_day == 1 && diff_year == 0) {
            // return _timestamp.calendar(null, { lastDay: '[Yesterday]' });
            return 'Yesterday'
        } else if (diff_day < 8 && diff_year == 0) {
            return _timestamp.format("ddd")
        } else if (diff_year == 0 && (diff_day <= 365 || diff_day <= 366)) {
            return _timestamp.format("DD/MM")
        } else {
            return _timestamp.format("DD/MM/YY")
        }
    },
    checkType(type, created_at = new Date()) {
        // console.log("🚀 ~ file: date.helper.js ~ line 29 ~ checkType ~ type", type)
        let today = new Date();
        switch (type) {
            case 'today': return { startDate: dayjs().format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") };
            case '1d':
                let yesterday = new Date();
                yesterday.setDate(today.getDate() - 1);
                return { startDate: dayjs(yesterday).format("YYYY-MM-DD"), endDate: dayjs(yesterday).format("YYYY-MM-DD") };
            case '7d':
                let last7days = new Date();
                last7days.setDate(today.getDate() - 6);
                return { startDate: dayjs(last7days).format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") };
            case '30d':
                let last30days = new Date();
                last30days.setDate(today.getDate() - 29);
                return { startDate: dayjs(last30days).format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") };
            case 'alltime':
                let alltime = new Date(created_at);
                return { startDate: dayjs(alltime).format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") };
            default:
                return { startDate: dayjs().format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") }
        }
    },
    checkTypeReport(type, created_at = new Date()) {
        // console.log("🚀 ~ file: date.helper.js ~ line 29 ~ checkType ~ type", type)
        let today = new Date();
        let yesterday = new Date();
        yesterday.setDate(today.getDate() - 1);
        switch (type) {
            case 'today': return { startDate: dayjs().format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") };
            case '1d':
                return { startDate: dayjs(yesterday).format("YYYY-MM-DD"), endDate: dayjs(yesterday).format("YYYY-MM-DD") };
            case '7d':
                let last7days = new Date();
                last7days.setDate(today.getDate() - 7);
                return { startDate: dayjs(last7days).format("YYYY-MM-DD"), endDate: dayjs(yesterday).format("YYYY-MM-DD") };
            case '30d':
                let last30days = new Date();
                last30days.setDate(today.getDate() - 30);
                return { startDate: dayjs(last30days).format("YYYY-MM-DD"), endDate: dayjs(yesterday).format("YYYY-MM-DD") };
            case 'alltime':
                let alltime = new Date(created_at);
                return { startDate: dayjs(alltime).format("YYYY-MM-DD"), endDate: dayjs(yesterday).format("YYYY-MM-DD") };
            default:
                return { startDate: dayjs().format("YYYY-MM-DD"), endDate: dayjs().format("YYYY-MM-DD") }
        }
    }
};
