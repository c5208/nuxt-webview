

const modalConfirm = ($bvModal, options = {}) => {
    return new Promise((resolve, reject) => {
        let name = options.name ? options.name : 'please confirm delete'
        $bvModal
            .msgBoxConfirm(name, {
                title: options.title ? options.title : 'Title',
                size: options.size ? options.size : "md",
                buttonSize: options.buttonSize ? options.buttonSize : "sm",
                okVariant: options.okVariant ? options.okVariant : "danger",
                okTitle: options.okTitle ? options.okTitle : 'Yes',
                cancelTitle: options.cancelTitle ? options.cancelTitle : 'No',
                footerClass: options.footerClass ? options.footerClass : "p-2",
                hideHeaderClose: options.hideHeaderClose ? options.hideHeaderClose : false,
                centered: options.centered ? options.centered : true,
            })
            .then(async (value) => {
                resolve(value)
            })
            .catch((err) => {
                resolve(false)
            });
    })
}

const validateFail = ($swal, options = {}) => {

    return new Promise((resolve, reject) => {
        let icon = "<i class='fas fa-exclamation-triangle pr-3 font-caution'></i>"
        $swal({

            title: `<h5>  ${options.title ? icon + options.title : icon} Unable to save data  </h5>`,
            text: options.text ? options.text : ' Please make sure all required fields are filled out correctly.',
            showCancelButton: true,
            showConfirmButton: false,
            cancelButtonText: "Close",
            allowOutsideClick: false,
        }).then(result => {
            resolve(result)

        });
        resolve(false)
    })

}

const confirm = ($swal, options = {}) => {

    return new Promise((resolve, reject) => {
        try {
            let icon = "<i class='fas fa-exclamation-triangle pr-3 font-caution'></i>"

            $swal({
                //title: options.cssText == true ? icon + `<h4><strong>${options.title}</strong></h4>`  : icon + `Confirm`,
                // text: options.text ? options.text : 'Are you sure you want to save changes ?',
                // html: options.html ? options.html : '',
                title: options.title ? `<h5 class=''><strong> ${options.title}</strong></h5>` : `<h5 class=''><strong> Save change</strong></h5>`,
                html: options.text ? `<div class="align-center border-bottom"> <p class="modal-text mt-0 mb-4">${options.text}</p>  </div> ` : `<div class="align-center border-bottom"> <p class="modal-text mt-0 mb-4">Are you sure you want to save changes ?</p>  </div> `,
                width: options.width ? options.width : `38em`,
                showCancelButton: true,
                showConfirmButton: true,
                cancelButtonText: options.cancelButtonText ? options.cancelButtonText : "Cancel",
                confirmButtonText: options.confirmButtonText ? options.confirmButtonText : "Confirm",
                allowOutsideClick: false,

            }).then(result => {
                resolve(result.isConfirmed)
            });
        } catch (err) {
            resolve(false)
        }

    })

}

const _delete = ($swal, options = {}) => {

    return new Promise((resolve, reject) => {
        try {
            let icon = "<div class='modal-round-icon'><span class='icon-img icon-fill-delete'></span></div>"
            // let icon = "<i class='fas fa-exclamation-triangle pr-3 font-primary'></i>"
            $swal({
                title: options.title ? `<div class="d-flex">${icon} <h5 class='modal-title-delete mt-1'>${options.title}</h5></div>` : `<div class="d-flex">${icon} <h5 class='modal-title-delete mt-1'>Delete</h5></div>`,
                //text: options.text ? `${options.text}` : 'please confirm delete',
                showCancelButton: true,
                showCloseButton: false,
                position: 'center',
                html: options.text ? `<div class="align-center border-bottom"> <p class="modal-text">${options.text}</p>  </div> ` : `<div class="align-center border-bottom"> <p class="modal-text">Are you sure you want to delete this items ?</p>  </div>  `,
                confirmButtonText: options.confirmButtonText ? options.confirmButtonText : `Delete`,
                cancelButtonText: options.cancelButtonText ? options.cancelButtonText : "Cancel",

            }).then(result => {

                resolve(result.isConfirmed)

            });
        } catch (error) {
            resolve(false)
        }


    })

}

const warning = ($swal, options = {}) => {

    return new Promise((resolve, reject) => {
        try {
            let icon = "<div class='modal-round-icon-alert'><span class='icon-img icon-fill-alert'></span></div>"
            // let icon = "<i class='fas fa-exclamation-triangle pr-3 font-caution'></i>"

            $swal({
                // title: options.title ?  icon + options.title : `<h4><strong>Please check your information again ?</strong></h4>`,
                //title: options.cssText == true ? icon + `<h4><strong>${options.title}</strong></h4>`  : icon + `Confirm`,
                // text: options.text ? options.text : '',
                // html: options.html ? options.html : '',
                // icon: 'warning',
                title: options.title ? icon + `<h5 class='modal-title-alert  mt-1'>${options.title}</h5>` : icon + `<h5 class='modal-title-alert  mt-1'>Leave without saving</h5>`,
                html: options.text ? `<div class="align-center border-bottom"> <p class="modal-text  mb-3">${options.text}</p>  </div> ` : `<div class="align-center border-bottom"> <p class="modal-text mt-2 mb-4">Are you sure want to leave this page ?</p>  </div> `,
                width: options.width ? options.width : `38em`,
                showCancelButton: true,
                showConfirmButton: options.hideConfrim ? false : true,
                confirmButtonText: options.confirmButtonText ? options.confirmButtonText : `Confirm`,
                cancelButtonText: options.cancelButtonText ? options.cancelButtonText : "Cancel",


            }).then(result => {
                resolve(result.isConfirmed)
            });
        } catch (err) {
            resolve(false)
        }

    })

}

export default { modalConfirm, validateFail, confirm, _delete, warning };