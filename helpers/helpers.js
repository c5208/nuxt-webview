import Vue from "vue";
import date from "@helpers/date.helper";
import ReplyMessage from "@helpers/ReplyMessage.helper";
import broadcast from "@helpers/BroadcastReport.helper";
import report from "@helpers/report.helper"
import FunctionHelper from "@helpers/FunctionHelper.helper.js";
import modal from "@helpers/modalconfirm.helper.js"
import alert from "@helpers/alert.helper.js"
import  notify from "@helpers/notify.helper.js"
const helpers = {
    date,
    ReplyMessage,
    FunctionHelper,
    broadcast,
    modal,
    alert,
    notify,
    report
}
const plugin = {
    install() {
      Vue.prototype.$helpers = helpers;
    }
  };
  
  Vue.use(plugin);