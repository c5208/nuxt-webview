
import { Joi } from 'vue-joi-validation';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash'
import defaultImgUser from "@/assets/images/icons/icon-profile.svg";
import defaultImgGroup from "@/assets/images/icons/icon-group.svg";
import defaultImgBot from "@/assets/images/icons/icon-profile-bot-2.svg";

export default {
    copyToClipboard(val) {
        const pattern = /^((http|https|ftp|\/):\/\/)/;
        const baseurl = window.location.href;
        const arr = baseurl.split('/');

        if (!pattern.test(val)) {
            val = val.replace(/(^\w+:|^)\/\//, '');
            val = `${arr[0]}//${val}`;
        }
        const selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    },
    validEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    async readFile(file) {
        return new Promise((resolve, reject) => {
            try {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = (evt) => {
                    let extension = file.name.split(".")[file.name.split(".").length - 1].toLowerCase()
                    let uuid = uuidv4();
                    let isImage = file.type.indexOf('image') === -1 ? false : true;
                    let isVideo = file.type.indexOf('video') === -1 ? false : true;
                    let base64 = reader.result.split(",").pop()
                    let category = isImage ? 'image' : isVideo ? 'video' : 'file';
                    if (isImage) {
                        let img = new Image();
                        img.onload = function () {

                            let resolution = { width: img.width, height: img.height }
                            let url = URL.createObjectURL(file)
                            resolve({ tempId: uuid, extension: extension, resolution: resolution, url: url, base64: base64, size: file.size, type: file.type, name: file.name, lastModified: file.lastModified, isImage: true, originalContentUrl: url, category })

                        };
                        img.src = evt.target.result;
                    } else if (isVideo) {
                        let thumbnailVideoImageBuffer = ""
                        let thumbnailVideoImageUrl = ""
                        // 
                        let resolution = { width: 0, height: 0 }
                        var dataUrl = reader.result;
                        var videoId = "getResolutionVideo";
                        var $videoEl = $('<video id="' + videoId + '"></video>');
                        $("body").append($videoEl);
                        $videoEl.attr('src', dataUrl);

                        var videoTagRef = $videoEl[0];
                        videoTagRef.addEventListener('loadedmetadata', function (e) {
                            console.log(videoTagRef.videoWidth, videoTagRef.videoHeight);
                            resolution.width = videoTagRef.videoWidth
                            resolution.height = videoTagRef.videoHeight
                            $(`#${videoId}`).remove();
                            let url = URL.createObjectURL(file)
                            resolve({ tempId: uuid, extension: extension, resolution: resolution, url: url, originalContentUrl: url, base64: base64, size: file.size, type: file.type, name: file.name, lastModified: file.lastModified, isVideo: true, thumbnailVideoImageBuffer: thumbnailVideoImageBuffer, thumbnailVideoImageUrl: thumbnailVideoImageUrl, category })
                        });
                        // 
                    }

                    else {
                        resolve({ tempId: uuid, extension: extension, url: "", base64: base64, size: file.size, type: file.type, name: file.name, lastModified: file.lastModified, isDoc: true, category })
                    }

                };
            } catch (error) {
                console.log(error)
                resolve({ url: "", base64: "" })
            }

        })
    },
    async getImageDimensions(base64) {
        return new Promise(function (resolved, rejected) {
            var i = new Image();
            i.onload = async function () {
                const base64Response = await fetch(base64);
                const blob = await base64Response.blob();
                let name = `capture-${Date.now()}.png`
                let _base64 = base64.split(",").pop()
                let uuid = uuidv4();
                let size = blob.size
                let type = blob.type
                const url = URL.createObjectURL(blob);

                const data = { tempId: uuid, resolution: { width: _.cloneDeep(i.width), height: _.cloneDeep(i.height) }, base64: _base64, url: url, originalContentUrl: url, size: size, originalname: name, name: name, type: type, isImage: true, category: 'image' }
                resolved(data);
            };
            i.src = base64;
        });
    },
    validateURL(url, ex ) {
        // var expression = ex ? ex : /^(?:https:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm
        var expression = ex ? ex : /^(?:http(s?):\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#%[\]@!\$&'\(\)\*\+,;=.]+$/gm
        var regexp = new RegExp(expression);
        return regexp.test(url);
    },
    validateForm(key, form, validateSchema) {
        try {
            let result = true;
            let validate = Joi.validate(form, validateSchema, {
                abortEarly: false, // include all errors
                allowUnknown: true,
                stripUnknown: true,
            });
            validate = JSON.parse(JSON.stringify(validate));

            if (!validate.error) return result;
            validate.error.details.forEach((row) => {
                if (key === row.context.key) result = false;
            });

            return result;
        } catch (error) {
            console.log(error)
            return false;
        }
    },
    setFileSize(size) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (size == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
        return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
    },

    toHHMMSS(time) {
        var sec_num = parseInt(time, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        return hours + ':' + minutes + ':' + seconds;
    },
    addComma(value) {
        if (value) {
            value = value.toString();
            return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return value
        }
    },
    checkImageGroup(link) {
        return _.isEmpty(link) == true ? defaultImgGroup : link;
    },
    checkImage(link) {
        return _.isEmpty(link) == true ? "https://asset.chatcone.net/images/default/default_people.png" : link;
    },
    checkImageBot(link) {
        return _.isEmpty(link) == true ? defaultImgBot : link;
    },
    checkData(data) {
        //   return true
        return false
        if (data.hasOwnProperty("action")) {
            if (data.action.hasOwnProperty("data") || data.action.type == 'uri') {
                return true
            }
        }
        return false
    },
    returnValue(data) {
        //   return "1"
        if (data.hasOwnProperty("action")) {
            if (data.action.hasOwnProperty("data")) {
                try {
                    let getDat = JSON.parse(data.action.data)
                    return getDat.sub_bubble ? getDat.sub_bubble : ""
                } catch (error) {
                    return ""
                }
            } else if (data.action.type == 'uri') {
                return "uri"
            }
        }
        return ""
    },
    validateRegex(url, ex ) {
        var expression = ex
        var regexp = new RegExp(expression);
        return regexp.test(url);
    },
}

