
const alertError = ($swal, options = {}) => {
   
    return $swal(
        {
        icon: options.icon ? options.icon : "error",
        title: options.title ? options.title : "Error !",
        showConfirmButton: options.showConfirmButton ? options.showConfirmButton : false,
        showCloseButton: options.showCloseButton ? options.showCloseButton : true,
        confirmButtonColor : options.confirmButtonColor ? options.confirmButtonColor  : 'red'
    });

}

export default { alertError };